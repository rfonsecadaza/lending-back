TRESHOLD = 50000

VERDICT_DECLINED_TEXT = "Sorry, but the amount that you are applying for is far beyond we can give you. Try an amount smaller than $50.000."
VERDICT_PENDING_TEXT = "We are not sure if we can lend you that money. Please wait forever for our decision."
VERDICT_APPROVED_TEXT = "Congratulations! The ammount of money is so small that we don't have any problem to approve it."

VERDICT_SEVERITY_DECLINED = "error"
VERDICT_SEVERITY_PENDING = "warning"
VERDICT_SEVERITY_APPROVED = "success"