from flask import Flask
from flask_restful import Api, Resource
from flask_cors import CORS

import param

app = Flask(__name__)
api = Api(app)
CORS(app)

@app.route("/")
class LendingBack(Resource):
    def get(self, tax_id, business_name, requested_amount):
        # Set a verdict and severity according to ammount
        if(requested_amount > param.TRESHOLD):
            verdict = param.VERDICT_DECLINED_TEXT
            severity = param.VERDICT_SEVERITY_DECLINED
        elif(requested_amount < param.TRESHOLD):
            verdict = param.VERDICT_APPROVED_TEXT
            severity = param.VERDICT_SEVERITY_APPROVED
        else:
            verdict = param.VERDICT_PENDING_TEXT
            severity = param.VERDICT_SEVERITY_PENDING

        return {"verdict":verdict,"severity":severity}

api.add_resource(LendingBack,"/apply/<string:tax_id>/<string:business_name>/<int:requested_amount>")

if __name__ == "__main__":
    app.run(debug=True)

